+++
title = "API Gateways"
outputs = ["Reveal"]
+++

# API Gateways

### The Heart of Microservices

---

### Why API Gateway is important?

<span data-audio-text="This is the text sent to the text-to-speech generator">
Quote from Gartner, “Magic Quadrant for Full Life Cycle API Management”
</span>

{{% fragment %}}> It is impossible to provide the platform for any digital strategy, build ecosystems and run an effective API program, without full life cycle API management.{{% /fragment %}}

---

###### Magic Quadrant for Full Life Cycle API Management 2018

<img class="box" src="magicquadrant.png" height="550">

---

{{< slide id="selection" transition="zoom" transition-speed="fast" >}}

{{% section %}}

### API Gateway Selection Consideration

<ul>
{{% fragment %}}<li>Lifecycle Management</li>{{% /fragment %}}
{{% fragment %}}<li>Deployment Models</li>{{% /fragment %}}
{{% fragment %}}<li>Developer Tools</li>{{% /fragment %}}
{{% fragment %}}<li>Governance</li>{{% /fragment %}}
{{% fragment %}}<li>Monitoring</li>{{% /fragment %}}
{{% fragment %}}<li>Security</li>{{% /fragment %}}
</ul>  
{{% fragment %}}<span class="small">Scroll right &#8594; to continue or  
scroll down &#8595; to view details about  
selection consideration</span>{{% /fragment %}}

{{% note %}}
So what API Gateway available on the market should you use? We are going to compare some of the available API Gateways out there based on following factors
{{% /note %}}

---

### Lifecycle Management

After an API has been published, there is still a need to build and test new versions that enhance or add new functionality. The API Gateway should let you operate multiple API versions and multiple stages for each version simultaneously.

---

### Deployment Models

Depending on the organzation, they may choose the most suitable deployment model that complements their existing infrastructure. The 3 most common deployment models supported are <span class="white-bold">Cloud, On-Premises and Hybrid</span>.

---

### Developer Tools

Development Tools in the form of Developer Portal or Desktop IDE should be available to the developers as a onboarding tool. It should be integrated with the API Gateway so that developers can register their APIs and monitor their API usage and performance.

---

### Governance

Governance refers to the ability to <span class="white-bold">track APIs</span> across their lifecycle, audit their usage and the ability for business to set <span class="white-bold">policies</span> on their usage etc.

---

### Monitoring 

<span class="white-bold">Detailed analytics</span> on how the APIs are being used should be available to the operators to ensure the APIs are maintaining their <span class="white-bold">QoS</span>.

---

### Security

Last factor but not least, security is often the one of the most important requirements of an API Management platform. There should be multiple tools to <span class="white-bold">authorize</span> access to your APIs and <span class="white-bold">control service operation access</span>. There should be <span class="white-bold">protection</span> against DoS attacks, malware, access control violations etc.

<a href="#/3">&#8593; Go back</a>

{{% /section %}}

---

# API Gateways

---

{{< slide id="apigee" background="#37020A" transition="zoom" transition-speed="fast" >}}

{{% section %}}

<img src="apigee/apigee_logo-svg.png">

<span class="small">Scroll right &#8594; to continue or  
down &#8595; to view details about Apigee API Gateway</span>

---

### An Introduction to Apigee Edge

<iframe width="889" height="500" src="https://www.youtube.com/embed/jWwmWvhI40Q" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

### Main Products
* Edge for Public Cloud
*  Edge for Private Cloud
  *  Edge Gateway (aka API Services)
  *  Edge Analytics
  * Edge Developer Services portal
  * Edge Monetization Services (aka Developer Services Monetization)
* Edge Microgateway

---

# Deployment Models

{{% note %}}
Reference:
https://docs.apigee.com/api-platform/microgateway/2.5.x/overview-edge-microgateway.html
{{% /note %}}

---

### Same machine

<img src="apigee/Microgateway-Fig1.png" height="390">  
<span class="small">Edge Microgateway and the backend target APIs are installed on the same machine. The Edge Microgateway instance will be used to front multiple backend target applications</span>

---

### Different machine

<img src="apigee/Microgateway-Fig2.png" height="400">  
<span class="small">Edge Microgateway can be installed separated from backend target APIs</span>

---

### With load balancer

<img src="apigee/Microgateway-Fig3d.png" height="400">  
<span class="small">Edge Microgateway itself can be front-ended by a standard reverse proxy or load-balancer for SSL termination and/or load-balancing</span>

---

### Intranet deployment

<img src="apigee/Microgateway-Fig4.png" height="350">  
<span class="small">Edge Microgateway can be used to protect intranet traffic while protecting internet traffic with Apigee Edge.</span>

---

{{< slide background="#FFF" transition="zoom" transition-speed="fast" >}}

### <font color="black">Google Apigee with PCF</font>

<img src="apigee/apigee_edge_pcf.png" height="400">  

---

### Links

* [Apigee Homepage](https://apigee.com)
* [Creating API Proxy](https://docs.apigee.com/api-platform/fundamentals/build-simple-api-proxy)
* [Apigee Edge Analytics Services Overview](https://docs.apigee.com/api-platform/analytics/analytics-services-overview)
* [Edge for Private Cloud documentation](https://docs.apigee.com/private-cloud/versions)

<a href="#/5">&#8593; Go back</a>

{{% /section %}}

---

{{< slide id="ca-tech" background="#000" transition="zoom" transition-speed="fast" >}}

{{% section %}}

<img src="ca-tech/logo-ca-technologies.png" height="350">

<span class="small">Scroll right &#8594; to continue or  
down &#8595; to view details about CA Technologies API Gateway</span>

---

### Product Family Architecture

<img src="ca-tech/product_family_architecture.png" height="500">

---

### Main Products
* [CA API Gateway](https://www.ca.com/us/products/apim/gateway.html)
* [CA API Developer Portal](https://www.ca.com/us/products/apim/developer-portal.html)
* [CA Live API Creator](https://www.ca.com/us/products/apim/api-development.html)
* [CA Microgateway](https://www.ca.com/us/products/apim/microgateway.html) - Lightweight containerized API gateway
* [CA Mobile API Gateway](https://www.ca.com/us/products/apim/mobile-api-gateway.html) - Lightweight and low-latency API gateway that supports mobile app architecture
* [CA API Management SAAS](https://www.ca.com/us/products/apim/saas.html)

---

### CA API Management Demo: API Integration

<iframe width="889" height="500" src="https://www.youtube.com/embed/KQI2U9oTH7g" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

### Links

* [CA Tech Homepage](https://www.ca.com/us/)
* [CA API Gateway Datasheet 2018](ca-tech/ca-api-gateway-datasheet-2018.pdf)
* [CA API Gateway Documentation](https://docops.ca.com/ca-api-gateway/9-4/en)
* [CA API Developer Portal Documentaion](https://docops.ca.com/ca-api-developer-portal-enhanced-experience/4-2/en/)
* [CA Live API Creator Documentation](https://docops.ca.com/ca-live-api-creator/5-1/en/)

<a href="#/6">&#8593; Go back</a>

{{% /section %}}

---

{{< slide id="mulesoft" background="#FFF" transition="zoom" transition-speed="fast" >}}

{{% section %}}

<img src="mulesoft/mulesoft-logo.png" height="350">

<span class="small">Scroll right &#8594; to continue or  
down &#8595; to view details about Mulesoft API Gateway</span>

---

### <font color="#62A0DF">Anypoint Platform Overview</font>

<iframe width="889" height="500" src="https://www.youtube.com/embed/kYq-bR2IDUw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

### <font color="#62A0DF">Anypoint Platform</font>

<img src="mulesoft/anypoint-platform.png" height="500">

---

# <font color="#62A0DF">Deployment Scenarios</font>

{{% note %}}
Reference:
https://docs.mulesoft.com/runtime-manager/deployment-strategies
{{% /note %}}

---

### <font color="#62A0DF">Runtime Manager to CloudHub Deployment</font>

<img src="mulesoft/infrastructure-simple-cloud.png" height="400">

CloudHub is the platform as a service (PaaS) component of Anypoint Platform.

---

### <font color="#62A0DF">Cloud Console to Own Servers (Hybrid)</font>

<img src="mulesoft/infrastructure-hybrid.png" height="400">

Host Mule Runtime On-Premises and use Cloud Console to manage it.

---

### <font color="#62A0DF">On-Premises Console On-Premises Deployment</font>

<img src="mulesoft/infrastructure-onprem.png" height="400">

Complete On-Premises solution using Anypoint Platform Private Cloud Edition.

---

### <font color="#62A0DF">On-Premises Console to Pivotal Cloud Foundry Deployment</font>

<img src="mulesoft/infrastructure-pcf.png" height="400">

This deployment strategy is only available through the Anypoint Platform Private Cloud Edition

---

### <font color="#62A0DF">Main Products</font>
* [Mule](https://www.mulesoft.com/platform/mule) - Runtime Engine for connecting applications, data and services
* [Anypoint Studio](https://www.mulesoft.com/platform/studio) - Desktop IDE for building and testing APIs and integrations.
* [Anypoint Monitoring](https://www.mulesoft.com/platform/api/monitoring-anypoint)
* [Anypoint Management Center](https://www.mulesoft.com/platform/anypoint-management-center) - Administer Anypoint Platform, manage APIs and users, analyze traffic, monitor SLAs etc.
* [Anypoint Security](https://www.mulesoft.com/platform/api/anypoint-security-services)

---

### <font color="#62A0DF">Links</font>

* [Mulesoft Homepage](https://www.mulesoft.com/)
* [Anypoint Platform Private Cloud Documentation](https://docs.mulesoft.com/private-cloud/2.0/)
* [Anypoint Platform Datasheet](mulesoft/Anypoint_Platform_Datasheet.pdf)

<a href="#/7">&#8593; Go back</a>

{{% /section %}}

---

{{< slide id="tyk" background="#C1DED4" transition="zoom" transition-speed="fast" >}}

{{% section %}}

<img src="tyk/tyk_logo.png" height="350">  
<font color="black"><span class="small">Scroll right &#8594; to continue or  
down &#8595; to view details about Tyk API Gateway</span></font>

---

### <font color="#353571">Tyk Architecture</font>

<img src="tyk/tyk_architecture.png" height="550">

---

### <font color="#353571">Main Components</font>
<font color="black"><ul>
<li> **API Gateway** - Proxy that handles all our apps traffic.
<li> **Dashboard/Portal** - Interface to manage Tyk, display metrics and organize the APIs.
<li> **Pump** - Responsible for persisting the data of the metrics, and exporting to MongoDB
</ul></font>

---

### <font color="black">Tyk API Gateway Demo</font>

<iframe width="889" height="500" src="https://www.youtube.com/embed/AA3S5o3VT_0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

### <font color="black">Links</font>

* [Tyk Homepage](https://tyk.io/)
* [Tyk Documentation](https://tyk.io/docs/)
* [Tyk Benchmarks](https://tyk.io/features/tyk-benchmarks/)

<a href="#/8">&#8593; Go back</a>

{{% /section %}}

---

{{< slide id="kong" background="#142031" transition="zoom" transition-speed="fast" >}}

{{% section %}}

<img src="kong/kong-logo.gif" height="350">

<span class="small">Scroll right &#8594; to continue or  
down &#8595; to view details about Mulesoft API Gateway</span>

---

{{< slide id="kong" background="#142031" >}}

### Main Products
* **Kong Manager** - Manage APIs, consumers, plugins, certificates, upstream and downstream objects 
* **Kong Dev Portal** - Generate API documentation, create custom pages, manage API versions and secure developer access.
* **Kong Vitals** - Visual API Analytics to monitor your Kong health and understand the microservice API transactions traversing Kong.

---

### API & Microservices Management with Kong

<iframe width="889" height="500" src="https://www.youtube.com/embed/S6CeWL2qvl4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

### Links

* [Kong Enterprise EE Homepage](https://konghq.com/kong-enterprise-edition/)
* [Kong Documentation](https://docs.konghq.com/)
* [Kong Enterprise Documentation](https://docs.konghq.com/enterprise/)
* [Kong Hub](https://docs.konghq.com/hub/)

<a href="#/9">&#8593; Go back</a>

{{% /section %}}

---

{{< slide transition="convex" transition-speed="slow" >}}

## API Gateways Comparision Matrix

---

{{< slide transition="convex" transition-speed="slow" >}}

<table style="font-size: 0.32em">
    <thead>
        <tr style="color: #EBE7D4">
            <th></th>
            <th>Apigee</th>
            <th>CA Tech</th>
            <th>Mulesoft</th>
            <th>Tyk</th>
            <th>Kong</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><span style="color: white">Gartner Magic Quadrant</span></td>
            <td>Leader</td>
            <td>Leader</td>
            <td>Leader</td>
            <td>Niche Player</td>
            <td>Visionary</td>
        </tr>
        <tr>
            <td><span style="color: white">Lifecycle Management</span></td>
            <td><a href="https://docs.apigee.com/api-platform/publish/create-api-products">Resource Management, Revisions</a> and <a href="https://docs.apigee.com/api-platform/reference/policies/reference-overview-policy">Policies</a></td>
            <td><a href="https://docops.ca.com/ca-api-gateway/9-4/en/published-services-and-policies">Services and Policies</a></td>
            <td><a href="https://docs.mulesoft.com/api-manager/2.x/manage-versions-instances-concept">Manage  API Versions and Instances</a></td>
            <td><a href="https://tyk.io/docs/concepts/versioning/">Versioning and Policies</a></td>
            <td>Limited. <a href="https://github.com/Kong/kong/issues/402">Versioning not available </a></td>
        </tr>
        <tr>
            <td><span style="color: white">Deployment Models</span></td>
            <td><a href="https://apigee.com/about/blog/digital-business/api-best-practices-platform-deployment-models">Cloud, On-Premises and Hybrid</a>. <a href="https://apigee.com/about/solutions/pivotal-cloud-foundry-apigee">Support PCF</a></td>
            <td><a href="ca-tech/deploying-your-api-infrastructure.pdf">Cloud, On-Premises and Hybrid</a></td>
            <td><a href="https://docs.mulesoft.com/runtime-manager/deployment-strategies">Cloud, On-Premises with support for PCF and Hybrid</a></td>
            <td><a href="https://tyk.io/api-gateway/cloud/">Cloud</a> and <a href="https://tyk.io/api-gateway/on-premise/">On-Premises</a> and <a href="https://tyk.io/api-gateway/cloud/#multi-cloud">Multi-Cloud (supercede Hybrid)</a></td>
            <td>On-Premises currently, <a href="https://konghq.com/cloud/">Cloud offering in limited beta</a></td>
        </tr>
        <tr>
            <td><span style="color: white">Developer Tools</span></td>
            <td><a href="https://docs-new.apigee.com/new-versus-classic-edge#portal-feature-comparison">Developer Services portal</a></td>
            <td><a href="https://www.ca.com/us/products/apim/developer-portal.html">CA API Developer Portal</a></td>
            <td><a href="https://www.mulesoft.com/platform/studio">Anypoint Studio</a>, <a href="https://www.mulesoft.com/platform/anypoint-design-center">API Design Center</a>, <a href="https://www.mulesoft.com/platform/api/developer-portal">API Portal</a> ...</td>
            <td>Tyk Dashboard</td>
            <td><a href="https://konghq.com/api-dev-portal/">Dev Portal</a></td>
        </tr>
        <tr>
            <td><span style="color: white">Governance</span></td>
            <td>&#9733;&#9733;&#9733;</td>
            <td>&#9733;&#9733;&#9733;</td>
            <td>&#9733;&#9733;&#9733;</td>
            <td>&#9733;&#9733;&#9734;</td>
            <td>&#9733;&#9733;&#9734;</td>
        </tr>
        <tr>
            <td><span style="color: white">Monitoring</span></td>
            <td>&#9733;&#9733;&#9733;<br/>
            <a href="https://docs.apigee.com/api-monitoring">Features mainly for cloud</a></td>
            <td>&#9733;&#9733;&#9733;<br/><a href="https://www.ca.com/us/products/api-monitoring.html">Has performance management</a></td>
            <td>&#9733;&#9733;&#9733;<br/><a href="https://docs.mulesoft.com/monitoring/">Anypoint Monitoring</a>
            </td>
            <td>&#9733;&#9733;&#9734;<br/>
            <a href="https://tyk.io/docs/analyse/">Part of Dashboard</a>
            </td>
            <td>&#9733;&#9733;&#9734;<br/><a href="https://konghq.com/products/kong-vitals/">Kong Vitals</a></td>
        </tr>
        <tr>
            <td><span style="color: white">Security</span></td>
            <td>&#9733;&#9733;&#9733;<br/><a href="https://docs.apigee.com/sense/what-apigee-sense">Apigee Sense</a></td>
            <td>&#9733;&#9733;&#9733;<br/>
            <a href="https://www.ca.com/us/products/api-security.html">API Security</a></td>
            <td>&#9733;&#9733;&#9733;<br/><a href="https://www.mulesoft.com/platform/api/anypoint-security-services">Anypoint Security</a></td>
            <td>&#9733;&#9733;&#9734;<br/><a href="https://tyk.io/docs/security/">API security and protection against OWASP top 10</a></td>
            <td>&#9733;&#9733;&#9734;<br/>
            <a href="https://konghq.com/api-security/">API security</a> and <a href="https://docs.konghq.com/hub/">security plugins</a></td>
        </tr>
        <tr>
            <td><span style="color: white">Pricing</span></td>
            <td><a href="https://apigee.com/api-management/#/pricing">Link</a></td>
            <td><a href="https://www.ca.com/us/contact/sales.html">Contact</a>, est. High</td>
            <td><a href="https://www.mulesoft.com/contact">Contact</a>, est. High</td>
            <td><a href="https://tyk.io/pricing/compare-api-management-platforms/">Link</a></td>
            <td><a href="https://konghq.com/request-demo/">Contact</a></td>
        </tr>
        <tr>
            <td><span style="color: white">Footprint (On-Premises)</span></td>
            <td><a href="https://docs.apigee.com/private-cloud/v4.18.05/installation-requirements"> Very High</a></td>
            <td>High<sup><a href="https://docops.ca.com/ca-api-gateway/9-4/en/release-notes-9-4/requirements-and-compatibility/software-gateway-specifications">[1]</a>
            <a href="https://docops.ca.com/ca-api-developer-portal-enhanced-experience/4-1/en/set-up-and-maintenance/set-up-the-api-developer-portal/api-developer-portal-prerequisites">[2]</a>
            <a href="https://docops.ca.com/ca-live-api-creator/5-1/en/release-notes/installation-requirements-and-supported-platforms/#InstallationRequirementsandSupportedPlatforms-InstallationRequirements">[3]</a></sup>
            </td>
            <td><a href="https://docs.mulesoft.com/private-cloud/2.0/prereq-hardware">Medium</a></td>
            <td><a href="https://community.tyk.io/t/results-from-load-test/42/6">Low</a></td>
            <td><a href="https://docs.konghq.com/enterprise/0.34-x/deployment-guide/#production-system-requirements">Low</a></td>
        </tr>
        <tr>
            <td><span style="color: white">Asia Pac Offices</span></td>
            <td>Singapore</td>
            <td>Many cities inc. Singapore</td>
            <td>Singapore, Hongkong, Sydney, Melbourne</td>
            <td>Singapore</td>
            <td>None in AP</td>
        </tr>
    </tbody>
</table>
<span style="font-size: 0.4em">Live slides last updated 27th Nov 2018</span>

---

{{< slide transition="convex" transition-speed="slow" >}}

### Bookmark to keep up-to-date

See https://gitlab.com/slideshow/api-gateways/ for more details

---

{{< slide transition="convex" transition-speed="slow" >}}

### - - - End - - -