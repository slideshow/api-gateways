![pipeline status](https://gitlab.com/slideshow/api-gateways/badges/master/pipeline.svg)

### API Gateway Live Slides

Slides: https://slideshow.gitlab.io/api-gateways

TODOs:
- [ ] Add Axway
- [ ] Add WSO2
- [ ] Expand on existing slides for API Gateway companies